package game;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.image.ImageObserver;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Font;
import java.awt.FontMetrics;

public class Menu extends JPanel {

	private static final long serialVersionUID = 1L;
	private String snake = "Comum";
	private boolean selecionado = false;
	ImageObserver imageobserver;

	public Menu() {
		
	}
	
	// Método que desenha o menu inicial
	public void menuInicial(Graphics g) {
		ImageIcon logo = new ImageIcon("assets/snake-logo.jpg");
		Graphics2D g2 = (Graphics2D) g;
		// set graphics quality
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);

        g.setColor(Color.black);
        g.drawImage(logo.getImage(),  0 , 0 , 905, 700 , imageobserver);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 100)); 
		g2.drawString("SNAKE", 250, 100);

		g2.setFont(new Font("Mops", Font.PLAIN, 48));
		g2.drawString("enter to Start", 290, 575);
		g2.drawString("space to see the fruits", 190, 630);
	}
	
	// Método que desenha as instruções das frutas
	public void instructions(Graphics g)
	{
		ImageIcon logo = new ImageIcon("assets/snake-logo.jpg");
		Graphics2D g2 = (Graphics2D) g;
		// set graphics quality
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        
        g.drawImage(logo.getImage(),  0 , 0 , 905 , 700 , imageobserver);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 100)); 
		g2.drawString("Fruits", 250, 100);
		
        g.setFont(new Font("TimesRoman", Font.PLAIN, 50)); 
        g.setColor(Color.white);
		g2.drawString("SIMPLE FRUIT - Brancas", 10, 200);
		g.setColor(Color.green);
		g2.drawString("BIG FRUIT - Verdes", 10, 300);
		g.setColor(Color.red);
		g2.drawString("DECREASE FRUIT - Vermelhas", 10, 400);
		g.setColor(Color.cyan);
		g2.drawString("BOMB FRUIT - Ciano", 10, 500);
		g.setColor(Color.black);
		g2.drawString("space to return", 250, 600);
	}
	
	// Método que desenha o menu de seleção
	public void selection(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		// set graphics quality
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		
		ImageIcon logo = new ImageIcon("assets/snake-logo.jpg");
		g.drawImage(logo.getImage(),  0 , 0 , 905 , 700 , imageobserver);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 50));
		g2.drawString("Choose a Snake", 250, 100);
					
		Rectangle snakeComumButton = new Rectangle(50, 250, 200, 200);
		Rectangle snakeKittyButton = new Rectangle(350, 250, 200, 200);
		Rectangle snakeStarButton = new Rectangle(650, 250, 200, 200);
		
		ImageIcon snakeComum = new ImageIcon("assets/azul.png");
		ImageIcon snakeKitty = new ImageIcon("assets/laranja.png");
		ImageIcon snakeStar = new ImageIcon("assets/amarelo.png");
        
        g.drawImage(snakeComum.getImage(), 50, 250, 200, 200, imageobserver);
        g.drawImage(snakeKitty.getImage(), 350, 250, 200, 200, imageobserver);
        g.drawImage(snakeStar.getImage(), 650, 250, 200, 200, imageobserver);
        
        g.setColor(Color.blue);
        g.drawString("Comum", 50, 500);
        g2.draw(snakeComumButton);
        g.setColor(Color.orange);
        g.drawString("Kitty", 385, 500);
        g2.draw(snakeKittyButton);
        g.setColor(Color.pink);
        g.drawString("Star", 700, 500);
        g2.draw(snakeStarButton);
        
       
        
        selectSnake(g);
	}
	
	// Método que seleciona a snake
	public void selectSnake(Graphics g)
	{
	    Graphics2D g2 = (Graphics2D) g;
	    g2.setStroke(new BasicStroke(3));
		g.setColor(Color.white);
		if (snake == "Comum")
		{
			g.drawRect(50, 250, 200, 200);
		}
		else if (snake == "Kitty")
		{
			g.drawRect(350, 250, 200, 200);
		}
		else if (snake == "Star")
		{
			g.drawRect(650, 250, 200, 200);
		}

	}
	
	// Método que desenha a tela de gameover
	public void gameover(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        
        Font font1 = new Font("Helvetica", Font.BOLD, 100);
        FontMetrics metr1 = getFontMetrics(font1);

        g2.setColor(Color.red);
        g2.setFont(font1);
        g2.drawString("Game Over", (905 - metr1.stringWidth("Game Over")) / 2, 700 / 2);
        
        g2.setFont(new Font("Helvetica", Font.BOLD, 40));
        g2.drawString("Espaco para reiniciar", 190, 400);
        
        g2.dispose();
	}

	public boolean isSelecionado() {
		return selecionado;
	}

	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	public String getSnake() {
		return snake;
	}

	public void setSnake(String snake) {
		this.snake = snake;
	}
}

