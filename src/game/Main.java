package game;

import java.awt.Color;
import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		JFrame janela = new JFrame("Snake");
		TelaJogo telajogo = new TelaJogo();

		janela.setBounds(10, 10, 905, 700);
		janela.setBackground(Color.black);
		janela.setResizable(false);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		janela.setLocationRelativeTo(null);
		janela.add(telajogo);
		telajogo.GameLoop();
		
		
	}

}

