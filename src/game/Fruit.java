package game;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import java.util.Vector;
import game.TelaJogo.Pair;

public class Fruit {
	
	public Random random = new Random();
	private String type;
	private int xpos = random.nextInt(34);
	private int ypos = random.nextInt(23);
	private int [] fruitxpos = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300,
			325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575,
			600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int [] fruitypos = {75, 100, 125, 150, 175, 200, 225, 250,
			275, 300, 325, 350, 375, 400, 425, 450,
			475, 500, 525, 550, 575, 600, 625};
	
	public Fruit() {
		type = "Simple";
	}
	
	// Método que desenha a fruta
	public void drawFruit(Graphics g)
	{
		if (getType() == "Simple")
			g.setColor(Color.white);
		else if (getType() == "Big")
			g.setColor(Color.green);
		else if (getType() == "Bomb")
			g.setColor(Color.cyan);
		else if (getType() == "Decrease")
			g.setColor(Color.red);
		g.fillRect(this.getFruitxpos(this.getXpos()), this.getFruitypos(this.getYpos()), 26, 26);
	}
	
	// Método que gera um novo tipo de fruta
	public String newFruitType()
	{
		int aux = random.nextInt(10);
		if (aux >= 0 && aux <= 6)
			return "Simple";
		else if (aux >= 7 && aux <= 8)
			return "Big";
		else 
			if (random.nextInt(2) % 2 == 0)
				return "Bomb";
			else
				return "Decrease";
	}
	
	// Método que seta o novo tipo de fruta
	public void generateFruit(Snake snake, Vector<Pair> obstaclespositions)
	{
		boolean pos_is_val = false;
		while (pos_is_val == false) {
			this.setXpos();
			this.setYpos();
			pos_is_val = true;
			for (int i = 0; i < 70; i++)
				if (obstaclespositions.get(i).first == this.getFruitxpos(this.getXpos()) && this.getFruitypos(this.getYpos()) == obstaclespositions.get(i).second)
					pos_is_val = false;
			for (int i = 0; i < snake.getLengthofsnake(); i++)
				if (snake.getSnakexlength(i) == this.getFruitxpos(this.getXpos()) && this.getFruitypos(this.getYpos()) == snake.getSnakeylength(i))
					pos_is_val = false;
		}
		this.setType(this.newFruitType());
	}

	public int getFruitxpos(int index) {
		return fruitxpos[index];
	}


	public void setFruitxpos(int[] fruitxpos) {
		this.fruitxpos = fruitxpos;
	}


	public int getFruitypos(int index) {
		return fruitypos[index];
	}


	public void setFruitypos(int[] fruitypos) {
		this.fruitypos = fruitypos;
	}

	public int getXpos() {
		return xpos;
	}


	public void setXpos() {
		this.xpos = random.nextInt(34);
	}


	public int getYpos() {
		return ypos;
	}


	public void setYpos() {
		this.ypos = random.nextInt(23);
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}
}
