package game;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.ImageObserver;
import java.util.Vector;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Font;

public class TelaJogo extends JPanel implements KeyListener, ActionListener, Runnable {
	
	// Classe implementada para auxiliar no desenvolvimento do jogo
	public class Pair {
		public int first;
		public int second;
		public Pair() {}
		public Pair(int a, int b)
		{
			this.first = a;
			this.second = b;
		}
	}
	
	// Define qual a tela sendo mostrada
	private enum STATE {
		MENUINICIAL,
		SELECTIONMENU,
		INSTRUCTIONS,
		GAME,
		SAIR;
	}

	// INICIALIZAÇÃO DAS VARIÁVEIS
	private static final long serialVersionUID = 1L;
	private Snake snake;
	private Fruit fruit = new Fruit();
	private Player player = new Player();
	private STATE state;
	private Menu menu = new Menu();
	public Vector<Pair> obstaclespositions = new Vector<Pair>();
	ImageObserver imageobserver;
	
	
	public TelaJogo()
	{
		state = STATE.MENUINICIAL;
		fruit = new Fruit();
		player = new Player();
		init();
		
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		define_posicoes_obstaculos();
		new Thread(this).start();
	}
	
	// Thread que gera as frutas
	@Override
	public void run()
	{
		while (getState() != STATE.GAME)
		{
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		while (getState() == STATE.GAME)
		{
			if (fruit.getType() == "Bomb")
			{
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				fruit.generateFruit(snake, obstaclespositions);
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	// Método chamado ao iniciar o gameplay
	public void init()
	{
		
		if (menu.getSnake() == "Comum")
			snake = new SnakeComum();
		else if (menu.getSnake() == "Kitty")
			snake = new SnakeKitty();
		else if (menu.getSnake() == "Star")
			snake = new SnakeStar();
		snake.setIs_alive(true);
		snake.setLengthofsnake(3);
		snake.setRight(false);
		snake.setLeft(false);
		snake.setUp(false);
		snake.setDown(false);
		player.setScore(0);
		
		snake.setSnakexlength(2, 75);
		snake.setSnakexlength(1, 100);
		snake.setSnakexlength(0, 125);

		snake.setSnakeylength(0, 100);
		snake.setSnakeylength(1, 100);
		snake.setSnakeylength(2, 100);
	}
	
	public void drawScoreBoard(Graphics g)
	{
		g.setColor(Color.black);
		g.fillRect(825, 15, 50, 45);
		g.setColor(Color.white);
		g.drawRect(24, 10, 851, 55);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 24)); 
		g.drawString("SnakeGame", 80, 45);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 14));
		g.drawString("Score: "+ player.getScore(), 780, 30);
		g.drawString("Length: "+ snake.getLengthofsnake(), 780, 50);
	}
	
	public void drawGameBoard(Graphics g)
	{
		// Desenha a borda
		g.setColor(Color.WHITE);
		g.drawRect(24, 74, 851, 577);
		
		// Desenha o background
		g.setColor(Color.black);
		g.fillRect(25, 75, 850, 575);
		
		// Desenha o grid
		g.setColor(Color.white);
		for (int i = 50; i <= 850; i += 25)
			g.drawLine(i, 75, i, 650);
		for (int i = 100; i <= 625; i += 25)
			g.drawLine(25, i, 875, i);
		
		// Desenha os obstaculos
		g.setColor(Color.cyan);
			// Desenha os obstaculos |__
		for (int i = 100, j = 150; i < 225; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 100, j = 175; j < 275; j += 25)
			g.fillRect(i, j, 23, 23);
		
		for (int i = 675, j = 150; i < 775; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 775, j = 150; j < 275; j += 25)
			g.fillRect(i, j, 23, 23);
		
		for (int i = 100, j = 550; i < 225; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 100, j = 525; j > 425; j -= 25)
			g.fillRect(i, j, 23, 23);
		
		for (int i = 675, j = 550; i < 775; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 775, j = 550; j > 425; j -= 25)
			g.fillRect(i, j, 23, 23);
		
			// Desenha os obstaculos --- 
		for (int i = 225, j = 275; i < 350; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 550, j = 275; i < 675; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 225, j = 425; i < 350; i += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 550, j = 425; i < 675; i += 25)
			g.fillRect(i, j, 23, 23);
		
			// Desenha os obstaculos ||
		for (int i = 425, j = 275; j < 450; j += 25)
			g.fillRect(i, j, 23, 23);
		for (int i = 450, j = 275; j < 450; j += 25)
			g.fillRect(i, j, 23, 23);
	}
	
	// Método que desenha a tela
	public void paint(Graphics g)
	{
		if (state == STATE.GAME)
		{
			if (snake.getIs_alive() == false)
				return;
			
			// Limpando a tela
			g.setColor(Color.black);
			g.fillRect(0, 0, 905, 700);
			
			// Atualizando a posição da cobra
			snake.updateSnake();
			
			// Desenhando a tela do jogo
			drawScoreBoard(g);
			drawGameBoard(g);
			snake.drawSnake(g);
			
			// Checando colisões
			snake.checkCollisionItself();
			snake.checkCollisionObstacles(obstaclespositions);
			if (snake.checkCollisionFruit(fruit))
			{
				snake.comeuFruta(fruit, player);
				fruit.generateFruit(snake, obstaclespositions);
			}
			fruit.drawFruit(g);
			
			if (snake.getIs_alive() == false)
				menu.gameover(g);
		}
		else if (state == STATE.MENUINICIAL)
			menu.menuInicial(g);
		else if (state == STATE.INSTRUCTIONS)
			menu.instructions(g);
		else if (state == STATE.SELECTIONMENU)
			menu.selection(g);
		g.dispose();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_ESCAPE)
			System.exit(1);
		
		// Game STATE
		if (state == STATE.GAME)
		{
			if (key == KeyEvent.VK_SPACE)
			{
				if (snake.getIs_alive() == true)
					return;
				state = STATE.SELECTIONMENU;
				repaint();
			}
			else if (key == KeyEvent.VK_RIGHT)
			{
				if (!snake.getLeft())
					snake.setRight(true);
				else
					snake.setRight(false);
				snake.setUp(false);
				snake.setDown(false);
			}
			else if (key == KeyEvent.VK_LEFT)
			{
				if (!snake.getRight())
					snake.setLeft(true);
				else
					snake.setLeft(false);
				snake.setUp(false);
				snake.setDown(false);
			}
			else if (key == KeyEvent.VK_UP)
			{
				if (!snake.getDown())
					snake.setUp(true);
				else
					snake.setUp(false);
				snake.setLeft(false);
				snake.setRight(false);
			}
			else if (key == KeyEvent.VK_DOWN)
			{
				if (!snake.getUp())
					snake.setDown(true);
				else
					snake.setDown(false);
				snake.setLeft(false);
				snake.setRight(false);
			}
		}
		// MENUINICIAL STATE
		else if (state == STATE.MENUINICIAL)
		{
			if (key == KeyEvent.VK_ENTER)
			{
				state = STATE.SELECTIONMENU;
				repaint();
			}
			else if (key == KeyEvent.VK_SPACE)
			{
				state = STATE.INSTRUCTIONS;
				repaint();
			}
		}
		// INSTRUCTIONS STATE
		else if (state == STATE.INSTRUCTIONS)
		{
			if (key == KeyEvent.VK_SPACE)
			{
				state = STATE.MENUINICIAL;
				repaint();
			}
		}
		// SELECTIONMENU STATE
		else if (state == STATE.SELECTIONMENU)
		{
			if (key == KeyEvent.VK_SPACE)
			{
				state = STATE.MENUINICIAL;
			}
			else if (key == KeyEvent.VK_ENTER)
			{
				state = STATE.GAME;
				init();
			}
			else if (key == KeyEvent.VK_RIGHT)
			{
				if (menu.getSnake() == "Comum")
					menu.setSnake("Kitty");
				else if (menu.getSnake() == "Kitty")
					menu.setSnake("Star");
				else if (menu.getSnake() == "Star")
					menu.setSnake("Comum");
			}
			else if (key == KeyEvent.VK_LEFT)
			{
				if (menu.getSnake() == "Kitty")
					menu.setSnake("Comum");
				else if (menu.getSnake() == "Star")
					menu.setSnake("Kitty");
				else if (menu.getSnake() == "Comum")
					menu.setSnake("Star");
			}
			repaint();
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
				
	}

	@Override
	public void keyTyped(KeyEvent arg0) {

	}
	
	// GAMELOOP
	public void GameLoop()
	{
		while (state != STATE.SAIR)
		{
			while (state == STATE.MENUINICIAL || state == STATE.INSTRUCTIONS || state == STATE.SELECTIONMENU)
				System.out.printf("");
			init();
			while (state == STATE.GAME) {
				System.out.printf("");
				while(snake.getIs_alive() == true)
				{
					repaint();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	// Método que armazena as posições dos obstaculos
	public void define_posicoes_obstaculos()
	{
		for (int i = 100, j = 150; i < 225; i += 25)
			obstaclespositions.add(new Pair(i, j));
		
		for (int i = 100, j = 175; j < 275; j += 25)
			obstaclespositions.add(new Pair(i, j));
		
		for (int i = 675, j = 150; i < 775; i += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 775, j = 150; j < 275; j += 25)
			obstaclespositions.add(new Pair(i, j));
		
		for (int i = 100, j = 550; i < 225; i += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 100, j = 525; j > 425; j -= 25)
			obstaclespositions.add(new Pair(i, j));
		
		for (int i = 675, j = 550; i < 775; i += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 775, j = 550; j > 425; j -= 25)
			obstaclespositions.add(new Pair(i, j));
		
		for (int i = 225, j = 275; i < 350; i += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 550, j = 275; i < 675; i += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 225, j = 425; i < 350; i += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 550, j = 425; i < 675; i += 25)
			obstaclespositions.add(new Pair(i, j));
		
		for (int i = 425, j = 275; j < 450; j += 25)
			obstaclespositions.add(new Pair(i, j));
		for (int i = 450, j = 275; j < 450; j += 25)
			obstaclespositions.add(new Pair(i, j));
	}

	public Snake getSnake() {
		return snake;
	}

	public void setSnake(Snake snake) {
		this.snake = snake;
	}

	public Fruit getFruit() {
		return fruit;
	}

	public void setFruit(Fruit fruit) {
		this.fruit = fruit;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public STATE getState() {
		return state;
	}

	public void setState(STATE state) {
		this.state = state;
	}
}

