package game;

public class SnakeComum extends Snake {
	
	private String type;
	
	public SnakeComum()
	{
		this.type = "Comum";
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getType()
	{
		return this.type;
	}
	
}
