package game;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;

import game.TelaJogo.Pair;

public class SnakeKitty extends Snake {
	private String type;
	
	public SnakeKitty() {
		this.type = "Kitty";
	}
	
	@Override
	// Método que desenha a cobra kitty
	public void drawSnake(Graphics g)
	{
		g.setColor(Color.orange);
		for (int i = 0; i < this.getLengthofsnake(); i++)
			g.fillRect(this.getSnakexlength(i), this.getSnakeylength(i), 23, 23);
	}
	
	@Override
	// Método que checa colisões da cobra kitty com obstaculos
	public void checkCollisionObstacles(Vector<Pair> obstaclespositions){}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getType()
	{
		return this.type;
	}
}
