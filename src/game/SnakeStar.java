package game;

import java.awt.Color;
import java.awt.Graphics;

public class SnakeStar extends Snake {
	private String type;
	
	public SnakeStar() {
		type = "Star";
	}
	
	@Override
	// Método que desenha a cobra star
	public void drawSnake(Graphics g)
	{
		g.setColor(Color.pink);
		for (int i = 0; i < getLengthofsnake(); i++)
			g.fillRect(getSnakexlength(i), getSnakeylength(i), 23, 23);
	}
	
	@Override
	// Método que atualiza os atributos da cobra star
	// quando ela come uma fruta
	public void comeuFruta(Fruit fruit, Player player)
	{
		if (fruit.getType() == "Simple")
		{
			setLengthofsnake(getLengthofsnake() + 1);
			player.setScore(2);
		}
		else if (fruit.getType() == "Big")
		{
			setLengthofsnake(getLengthofsnake() + 1);
			player.setScore(4);
		}
		else if (fruit.getType() == "Decrease")
			setLengthofsnake(3);
		else if (fruit.getType() == "Bomb")
			setIs_alive(false);
	}
	
	public void setType(String type)
	{
		this.type = type;
	}
	
	public String getType()
	{
		return type;
	}
}
