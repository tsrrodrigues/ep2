package game;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Vector;
import game.TelaJogo.Pair;

public class Snake {
	
	private String type;
	private boolean is_alive = true;
    private int[] snakexlength = new int[750];
    private int[] snakeylength = new int[750];
    private boolean left = false;
    private boolean right = false;
    private boolean up = false;
    private boolean down = false;
    private int lengthofsnake = 3;
    
    
    public Snake(){
    	this.type = "Comum";
    }
    
    // Método que atualiza a posição da cobra
    public void updateSnake()
    {
    	if (getRight() == true)
		{
			for (int i = getLengthofsnake()-1; i >= 0; i--)
				setSnakeylength(i+1, getSnakeylength(i));
			for (int i = getLengthofsnake(); i >= 0; i--)
			{
				if (i == 0)
					setSnakexlength(i, getSnakexlength(i) + 25);
				else
					setSnakexlength(i, getSnakexlength(i-1));
				if (getSnakexlength(i) > 850)
					setSnakexlength(i, 25);
			}
		}
		else if (getLeft() == true)
		{
			for (int i = getLengthofsnake()-1; i >= 0; i--)
				setSnakeylength(i+1, getSnakeylength(i));
			for (int i = getLengthofsnake(); i >= 0; i--)
			{
				if (i == 0)
					setSnakexlength(i, getSnakexlength(i) - 25);
				else
					setSnakexlength(i, getSnakexlength(i-1));
				if (getSnakexlength(i) < 25)
					setSnakexlength(i, 850);
			}
		}
		else if (getDown() == true)
		{
			for (int i = getLengthofsnake()-1; i >= 0; i--)
				setSnakexlength(i+1, getSnakexlength(i));
			for (int i = getLengthofsnake(); i >= 0; i--)
			{
				if (i == 0)
					setSnakeylength(i, getSnakeylength(i) + 25);
				else
					setSnakeylength(i, getSnakeylength(i-1));
				if (getSnakeylength(i) > 625)
					setSnakeylength(i, 75);
			}	
		}
		else if (getUp() == true)
		{
			for (int i = getLengthofsnake()-1; i >= 0; i--)
				setSnakexlength(i+1, getSnakexlength(i));
			for (int i = getLengthofsnake(); i >= 0; i--)
			{
				if (i == 0)
					setSnakeylength(i, getSnakeylength(i) - 25);
				else
					setSnakeylength(i, getSnakeylength(i-1));
				if (getSnakeylength(i) < 75)
					setSnakeylength(i, 625);
			}
		}
    }
    
 // Método que desenha a cobra
    public void drawSnake(Graphics g)
	{
		g.setColor(Color.blue);
		for (int i = 0; i < getLengthofsnake(); i++)
			g.fillRect(this.getSnakexlength(i), this.getSnakeylength(i), 23, 23);
	}
	
    // Método que checa colisões da cobra com ela mesma
	public void checkCollisionItself()
	{
		for (int i = 1; i < this.getLengthofsnake(); i++)
			if (this.getSnakexlength(0) == this.getSnakexlength(i) && this.getSnakeylength(0) == this.getSnakeylength(i))
				this.setIs_alive(false);
	}
	
	// Método que checa colisões da cobra com os obstaculos
	public void checkCollisionObstacles(Vector<Pair> obstaclespositions)
	{
		for (int i = 0; i < 70; i++)
			if(obstaclespositions.get(i).first == this.getSnakexlength(0) && obstaclespositions.get(i).second == this.getSnakeylength(0))
				this.setIs_alive(false);
	}
	
	// Método que checa colisões da cobra com as frutas
	public boolean checkCollisionFruit(Fruit fruit)
	{
		return (fruit.getFruitxpos(fruit.getXpos()) == this.getSnakexlength(0) && fruit.getFruitypos(fruit.getYpos()) == this.getSnakeylength(0));
	}
	
	// Método que atualiza os atributos da cobra quando ela come uma fruta
	public void comeuFruta(Fruit fruit, Player player)
	{
		if (fruit.getType() == "Simple")
		{
			this.setLengthofsnake(this.getLengthofsnake() + 1);
			player.setScore(1);
		}
		else if (fruit.getType() == "Big")
		{
			this.setLengthofsnake(this.getLengthofsnake() + 1);
			player.setScore(2);
		}
		else if (fruit.getType() == "Decrease")
			this.setLengthofsnake(3);
		else if (fruit.getType() == "Bomb")
			this.setIs_alive(false);
	}

	public boolean getIs_alive() {
		return this.is_alive;
	}

	public void setIs_alive(boolean is_alive) {
		this.is_alive = is_alive;
	}

	public int getSnakexlength(int index) {
		return snakexlength[index];
	}

	public void setSnakexlength(int index, int value) {
		this.snakexlength[index] = value;
	}

	public int getSnakeylength(int index) {
		return snakeylength[index];
	}

	public void setSnakeylength(int index, int value) {
		this.snakeylength[index] = value;
	}

	public boolean getLeft() {
		return left;
	}

	public void setLeft(boolean left) {
		this.left = left;
	}

	public boolean getRight() {
		return right;
	}

	public void setRight(boolean right) {
		this.right = right;
	}

	public boolean getUp() {
		return up;
	}

	public void setUp(boolean up) {
		this.up = up;
	}

	public boolean getDown() {
		return down;
	}

	public void setDown(boolean down) {
		this.down = down;
	}

	public int getLengthofsnake() {
		return lengthofsnake;
	}

	public void setLengthofsnake(int lengthofsnake) {
		this.lengthofsnake = lengthofsnake;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}