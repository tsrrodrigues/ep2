# Snake

Esse projeto teve como o objetivo o desenvolvimento de um
jogo da cobra usando o paradigma de programação orientado a objetos.

Possui as seguintes funcionalidades:

- Implementado as Snakes: **comum**, **kitty** e **star**
- Implementado as Frutas: **simples**, **big**, **decrease** e **bomb**
- Implementado o **GameLoop**
- Implementado **Herança** na classe SNAKE
- Implementado **Polimorfismo** nas classes filhas de SNAKE
- Implementado o **Encapsulamento** em todas as classes
- Implementado uma **Thread** que gera as frutas do jogo
- Implementado um **Menu** com interação com o usuário
- Implementado o **Diagrama de Classes**
- As snakes podem atravessar as paredes laterais do jogo, porém não os obstáculos, execeto a Snake Kitty

# O Projeto:
- Foi desenvolvido na IDE eclipse. Pode ser executado tanto pela IDE quanto pelo terminal
- Para ser executado é necessário possuir o java-11-openjdk-amd64 

# Como Jogar

1. O jogo possui um menu inicial com a opção de ver as frutas e com a opção de começar o jogo.
2. Caso deseje começãr o jogo, selecione a snake desejada usando as setas e a tecla enter.
3. A navegação no menu é permitida através das setas left e right e as teclas enter e space.
4. Para sair do programa é possível fechar a janela ou apertar esc.
5. Cada snake possui sua respectiva habilidade.
6. As frutas só irão reaparecer caso elas sejam comidas, exceto pela bomb fruit, que some depois de alguns segundos, aparecendo outro tipo de fruta.
7. Cada fruta contabiliza o score do jogador, tente obter o maior score possível.
8. A movimentação da cobra é realizada pelas setas do teclado.
9. Caso morra, para reiniciar aperte a tecla space

# Como compilar e executar

No terminal

1. Entre na pasta ep2 e digite o seguinte comando para compilar:

```javac -d bin src/game/*.java```

2. E o seguinte comando para executar:

```java -cp bin game.Main```

No Eclipse

1. File -> open projects from file system
2. Escolha a pasta ep2
3. Clique com o botão direito em cima do projeto e vá em  BuildPath -> Configure BuildPath
4. Em Source, inclua todas as pastas qeu estão dentro de ep2/src
5. Clique em Apply and Close
6. Clique com o botão direito em cima da pasta ep2 no eclipse e vá em  Run as -> Java Application

# EP2 - OO 2019.2 (UnB - Gama)

Turmas Renato e Carla
Data de entrega: 05/11/2019

## Descrição

Para este EP o objetivo será implementar o famoso jogo Snake (também conhecido como "jogo da cobrinha") é um jogo que ficou conhecido por diversas versões cuja versão inicial começou com o jogo Blockade de 1976, sendo feitas várias imitações em vídeo-games e computadores. No fim dos anos 90 foi popularizado em celulares da Nokia que vinham com o jogo já incluso.

O jogador controla uma longa e fina criatura que se arrasta pela tela, coletando comida (ou algum outro item), não podendo colidir com seu próprio corpo ou as "paredes" que cercam a área de jogo. Cada vez que a serpente come um pedaço de comida, seu rabo cresce, aumentando a dificuldade do jogo. O usuário controla a direção da cabeça da serpente (para cima, para baixo, esquerda e direita) e seu corpo segue.

Para este EP o jogo deve conter barreiras em seu cenário, além das especificações abaixo.

## Game Loop

Para o jogo deve ser implementado um game loop. Um game loop é um laço infinito que continua rodando enquanto o jogo estiver sendo executado. O game loop verifica periodicamente os inputs de usuário e realiza as ações requisitadas. Ele deve encerrar com a morte da Snake. Quando o game loop for encerrado deve aparecer uma tela de Game Over.

## Tipos de Snakes

Deve ser implementado ao menos 3 tipos de Snakes:
* **Comum:** A Snake classica, sem habilidades especiais.
* **Kitty:** Essa Snake tem as habilidades de atravessar as barreiras do jogo, mas não pode atravessar as bordas nem a si mesma.
* **Star:** Recebe o dobro de pontos ao comer as frutas.

## Colisões

As Snakes devem colidir com barreiras no interior do jogo, salvo a Snake Kitty que atravessa barreiras. Nenhuma cobra deve atravessar as bordas do jogo. Quando houver algum tipo de colisão, tanto nas barreiras, quanto elas mesmas, a Snake deve morrer.

## Frutas

As frutas são elementos que aparecem aleatoriamente e são os objetivos das Snakes. As frutas devem desaparecer em um tempo especifico e não devem aparecer mais de duas frutas por vez. Devem ser implementados ao menos 4 tipos de frutas:

* **Simple Fruit:** Fruta comum, dá um ponto e aumenta o tamanho da cobra.
* **Bomb Fruit:** Essa fruta deve levar a morte da Snake.
* **Big Fruit:** Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
* **Decrease Fruit:** Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.

**OBS.:** As frutas podem ser geradas por thread.

## Pontos

Os pontos são calculados de acordo com as frutas coletadas.

**OBS.:** Os pontos podem ser calculados a partir de thread.

## Orientações

O aluno deve escolher se deve implementar thread nos pontos, frutas ou ambos. Lembrando que ter thread faz parte da pontuação do EP.

Quando finalizado, o projeto deverá conter neste README as instruções de execução (comandos, menus, etc) e a lista de dependências (bibliotecas ou pacotes necessários para se executar o software).

## 5. Critérios de Avaliação

 A avaliação será realizada seguindo os seguintes critérios:

|   ITEM    |   COMENTÁRIO  |   VALOR   |
|------------------------|---------------------------------------------------------------------------------------------------------|---------|
|**Game Loop**| A aplicação deve ter implementados o Game Loop do Jogo com tela de Game Over ao final. |    1,0 |
|**Tipos de Snake e Frutas**| A aplicação deve ter implementados os 3 tipos de cobra e 4 tipos de fruta. |   1,0 |
|**Herança**| Utilização de Herança (que faça sentido).  |   1,0 |
|**Polimorfismo**|  No código deve haver, no mínimo, três casos de polimorfismo sendo utilizados.    |   1,0 |
|**Encapsulamento**| Utilização adequada de encapsulamento em todas as classes.    |   1,0 |
|**Modelagem**| Qualidade do código e dos relacionamentos entre classes.  |  0,5 |
|**Thread**| O jogo deve possuir ao menos uma thread.  |  0,5 |
|**Interface**| O programa deve possuir a interface Swing e possuir interação com o usuário |   1,0 |
|**Qualidade do Código**| Utilização de boas práticas como o uso de bons nomes, modularização e organização em geral.    |  1,0 |
|**UML**| O EP deve possuir um diagrama UML em sua documentação |  1,0 |
|**Repositório**|   Utilização correta do repositório, commits claros e frequentes. |   1,0 |
|      |    TOTAL   |   10  |
|**Pontuação Extra**| Possiveis pontos extras:<br/>- Ranking em arquivo com nome do jogador e quantidade de pontos<br/>- Fazer as cobras atravessarem as bordas do jogo aparecendo no outro lado<br/>- Testes automatizados|  0,5 (para cada funcionalidade) |
